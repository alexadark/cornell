<?php
//Template name: Staff page

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
add_action( 'wp_enqueue_scripts', 'cornell_enqueue_readmore' );

 $context   = Timber::get_context();
 $templates = array( 'staff-page.twig' );
 Timber::render( $templates, $context );
<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
add_action( 'wp_enqueue_scripts', 'cornell_enqueue_slick' );

 $context   = Timber::get_context();
 $templates = array( 'front-page.twig' );
 Timber::render( $templates, $context );
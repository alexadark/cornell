<?php
require_once __DIR__ . '/template-functions.php';
require_once __DIR__ . '/acf-options-page.php';

/*
 * Custom post Types
 */
require_once __DIR__ . '/custom-post-types.php';



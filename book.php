<?php
//Template name: Book

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
add_action( 'wp_enqueue_scripts', 'cornell_enqueue_slick' );
add_action( 'wp_enqueue_scripts', 'cornell_enqueue_hashtabber' );


 $context   = Timber::get_context();
 $templates = array( 'book.twig' );
 Timber::render( $templates, $context );
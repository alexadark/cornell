<?php
//Template name: Contact

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

 $context   = Timber::get_context();
 $templates = array( 'contact.twig' );
 Timber::render( $templates, $context );
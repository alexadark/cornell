import { menuLinksAnimation } from './utilities.js';

document.addEventListener('DOMContentLoaded', () => {
  const navigationContainer = document.querySelector(
      '.menu-main-navigation-container'
    ),
    headerSearchForm = document.getElementById('supapress-search-form'),
    menuLinks = document.querySelectorAll('#primary-menu > li.menu-item');

  document
    .getElementById('search-trigger-mobile')
    .addEventListener('click', e => {
      headerSearchForm.classList.toggle('show');
    });
  document.querySelector('.menu-trigger').addEventListener('click', e => {
    console.log('open');
    e.currentTarget.classList.toggle('is-active');
    navigationContainer.classList.toggle('show');
    menuLinksAnimation(menuLinks);
  });
});

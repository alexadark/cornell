<?php
//Template name: About

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
add_action( 'wp_enqueue_scripts', 'cornell_enqueue_hashtabber' );



 $context   = Timber::get_context();
 $templates = array( 'about.twig' );
 Timber::render( $templates, $context );
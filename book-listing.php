<?php
//Template name: Book Listing

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

 $context   = Timber::get_context();
 $templates = array( 'book-listing.twig' );
 Timber::render( $templates, $context );
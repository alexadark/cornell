import { $$, LightenDarkenColor } from './utilities';

const triggersOpenTiles = $$('.open-tiles');

function openTiles() {
  this.previousElementSibling.classList.toggle('show');
}

triggersOpenTiles.forEach(trigger => trigger.on('click', openTiles));

//Trending Now
const trendingItems = $$('.trending__item-wrap');

function addColorOnHover() {
  this.style.background = LightenDarkenColor(this.dataset.color, 20);
}
function removeColorOnMouseout() {
  this.style.background = this.dataset.image;
  this.style.backgroundSize = 'cover';
}

trendingItems.forEach(item => {
  item.on('mouseover', addColorOnHover);
  item.on('mouseout', removeColorOnMouseout);
});

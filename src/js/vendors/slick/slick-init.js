jQuery(document).ready(function($) {
  $('.book-carousel').slick({
    autoplay: true,
    speed: 600,
    prevArrow: '<a href="#" class="fa fa-arrow-left " ></a>',
    nextArrow: '<a href="#" class="fa fa-arrow-right " /></a>',
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  $('.header-carousel').slick({
    autoplay: true,
    speed: 1000,
    fade: true,
  });

  $('.journal-catalog__slider').slick({
    // autoplay: true,
    speed: 1000,
    // fade: true,
    prevArrow: '<a href="#" class="fa fa-chevron-left " ></a>',
    nextArrow: '<a href="#" class="fa fa-chevron-right " /></a>',
  });

  $('.news-tabs-slider').slick({
    autoplay: true,
    speed: 1000,
    prevArrow: '<a href="#" class="fa fa-arrow-left " ></a>',
    nextArrow: '<a href="#" class="fa fa-arrow-right " /></a>',
  });
});

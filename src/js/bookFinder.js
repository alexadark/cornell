import { $ } from './utilities';

const bookFinder = $('.book-finder');
const bookFinderIcon = $('.book-finder span');
const bookFinderTitle = $('.book-finder__title');
const bookFinderIconClasses = bookFinderIcon.classList;

const isSafari =
  navigator.vendor &&
  navigator.vendor.indexOf('Apple') > -1 &&
  navigator.userAgent &&
  navigator.userAgent.indexOf('CriOS') == -1 &&
  navigator.userAgent.indexOf('FxiOS') == -1;

// detect if it's mobile device https://coderwall.com/p/i817wa/one-line-function-to-detect-mobile-devices-with-javascript
const isMobileDevice = () =>
  typeof window.orientation !== 'undefined' ||
  navigator.userAgent.indexOf('IEMobile') !== -1;

/**
 * Positon the book finder checking if it's safari, as safari positionned differently
 */

const bookFinderInitialPosition = () => {
  if (isMobileDevice()) {
    if (window.innerWidth <= 480) {
      bookFinder.style.right = '-285px';
    } else {
      bookFinder.style.right = '-390px';
    }
  } else {
    isSafari
      ? (bookFinder.style.right = '-410px')
      : (bookFinder.style.right = '-352px');
  }
};

bookFinderInitialPosition();

//Open book finder on click anywhere on the element
bookFinder.on('click', openBookFinder);

//Close bookFInder on click only on title to allow click on the form
bookFinderTitle.on('click', closeBookFinder);

//Close bookFinder on click anywhere outside of the bookfinder
document.on('click', e => {
  if (!e.target.closest('.book-finder') && bookFinder.style.right === '0px') {
    closeBookFinder(e);
  }
});

/**
 * Open bookFinder if it's close: change position and Icon
 *
 *
 */
function openBookFinder() {
  if (bookFinder.style.right !== '0px') {
    bookFinder.style.right = 0;
    bookFinderIconClasses.replace('fa-search', 'fa-times');
  }
}
/**
 * Close bookFinder if it's open, stopPropagation to avoid the click goes to the ancestors of the title
 *
 */
function closeBookFinder(e) {
  if (bookFinder.style.right === '0px') {
    e.stopPropagation();
    bookFinderInitialPosition();
    bookFinderIconClasses.replace('fa-times', 'fa-search');
  }
}

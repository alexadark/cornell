jQuery('.staff__bio').readmore({
  speed: 400,
  moreLink: '<a class="staff__more" href="#">Read more</a>',
  lessLink: '<a class="staff__more close" href="#">Close</a>',
  collapsedHeight: 100,
  heightMargin: 20,
  embedCSS: false,
});

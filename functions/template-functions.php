<?php
function cornell_university_press_cart( $class ) {
	$html = '';
	//    if( function_exists( 'get_field' ) && !empty( get_field( 'uni_press_cart_link', 'options' ) ) ) {
	//        $link = get_field( 'uni_press_cart_link', 'options' );
	$html = "<div class='site-cart-link {$class}'><a href='#' target='_blank' title=''><span class='copy-cart'>My Cart</span><span class='fa-shopping-cart'></span></a></div>";
	//    }

	return $html;
}



function cornell_enqueue_slick() {

	wp_enqueue_script( 'slick', THEME_JS . 'slick.min.js', 'jquery', '1.2.0', true );
}

function cornell_enqueue_hashtabber() {

	wp_enqueue_script( 'hashtabber', THEME_JS . 'hashTabber.min.js', '', '2.2', true );

}

function cornell_enqueue_readmore() {

	wp_enqueue_script( 'readmore', THEME_JS . 'readmore.min.js', 'jquery', '2.2', true );

}
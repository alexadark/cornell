<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

 //Initialize theme constants
function cornell_init_constants() {
	$theme = wp_get_theme();

	define( 'THEME_NAME', $theme->get( 'Name' ) );
	define( 'THEME_URL', $theme->get( 'ThemeURI' ) );
	define( 'CORNELL_THEME_VERSION', $theme->get( 'Version' ) );
	define( 'TEXT_DOMAIN', $theme->get( 'TextDomain' ) );

	define( 'THEME_DIR', get_template_directory() );
	define( 'THEME_URI', get_template_directory_uri() );
	define( 'THEME_IMG', THEME_URI . '/dist/assets/img/' );
	define( 'THEME_JS', THEME_URI . '/dist/js/' );
	define( 'THEME_CSS', THEME_URI . '/dist/css/' );
}

cornell_init_constants();
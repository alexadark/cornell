<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'init', 'cornell_custom_post_types' );
function cornell_custom_post_types() {
	register_post_type(
		'subjects',
		array(
			'labels'             => array(
				'name'          => __( 'Subjects' ),
				'singular_name' => __( 'Subject' ),
			),
			'public'             => false,
			'hierarchical'       => true,
			'has_archive'        => false,
			'query_var'          => true,
			'capability_type'    => 'post',
			'show_in_nav_menus'  => true,
			'show_in_rest'       => true,
			'show_ui'            => true,
			'menu_icon'          => 'dashicons-admin-post',
			'supports'           => array( 'title', 'revisions', 'thumbnail' ),
			'publicly_queryable' => true,
		)
	);

	register_post_type(
		'series',
		array(
			'labels'             => array(
				'name'          => __( 'Series' ),
				'singular_name' => __( 'Serie' ),
			),
			'public'             => false,
			'hierarchical'       => true,
			'has_archive'        => false,
			'query_var'          => true,
			'capability_type'    => 'post',
			'show_in_nav_menus'  => true,
			'show_in_rest'       => true,
			'show_ui'            => true,

			'menu_icon'          => 'dashicons-images-alt',
			'supports'           => array( 'title', 'revisions', 'thumbnail', 'editor' ),
			'publicly_queryable' => true,
		)
	);
	register_post_type(
		'catalogs',
		array(
			'labels'             => array(
				'name'          => __( 'Catalogs' ),
				'singular_name' => __( 'Catalog' ),
			),
			'public'             => false,
			'hierarchical'       => true,
			'has_archive'        => true,
			'query_var'          => true,
			'capability_type'    => 'post',
			'show_in_rest'       => true,
			'show_in_nav_menus'  => true,
			'show_ui'            => true,
			'menu_icon'          => 'dashicons-media-document',
			'supports'           => array( 'title', 'revisions', 'thumbnail', 'editor' ),
			'publicly_queryable' => true,
		)
	);

	register_post_type(
		'journals',
		array(
			'labels'             => array(
				'name'          => __( 'Journals' ),
				'singular_name' => __( 'Journal' ),
			),
			'public'             => false,
			'hierarchical'       => true,
			'has_archive'        => false,
			'query_var'          => true,
			'capability_type'    => 'post',
			'show_in_nav_menus'  => true,
			'show_in_rest'       => true,
			'show_ui'            => true,
			'menu_icon'          => 'dashicons-media-document',
			'supports'           => array( 'title', 'revisions', 'thumbnail', 'editor' ),
			'publicly_queryable' => true,
		)
	);

	register_post_type(
		'imprints',
		array(
			'labels'             => array(
				'name'          => __( 'Imprints' ),
				'singular_name' => __( 'Imprint' ),
			),
			'public'             => false,
			'hierarchical'       => true,
			'has_archive'        => false,
			'query_var'          => true,
			'capability_type'    => 'post',
			'show_in_nav_menus'  => true,
			'show_in_rest'       => true,
			'show_ui'            => true,
			'menu_icon'          => 'dashicons-editor-paste-text',
			'supports'           => array( 'title', 'revisions', 'thumbnail', 'editor' ),
			'publicly_queryable' => true,
		)
	);

}
